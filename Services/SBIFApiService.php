<?php

namespace dsarhoya\DSYSBIFAPIClientBundle\Services;

use GuzzleHttp\Client;

/**
 * Description of SBIFApiService
 *
 * @author kito
 */
class SBIFApiService {
    const SBIF_BASE_URL = "http://api.sbif.cl/api-sbifv3/recursos_api/";
    const SBIF_DOLAR_SERVICE = "dolar";
    const SBIF_EURO_SERVICE = "euro";
    const SBIF_UF_SERVICE = "uf";
    
    const CHILEAN_PESO = "clp";
    const AMERICAN_DOLLAR = "usd";
    const EURO = "eur";
    const UF = "uf";
    
    private $http_client;
    private $request_params;
    private $em;
    
    public function __construct($api_key, $entity_manager) {
        $this->http_client = new Client(["base_uri" => self::SBIF_BASE_URL]);
        $this->request_params = ["formato"=>"json", "apikey"=>$api_key];
        $this->em = $entity_manager;
    }
    
    public function getCurrencyEquivalence($currency){
        switch ( $currency ){
            case self::CHILEAN_PESO:
                return 1;
            case self::AMERICAN_DOLLAR:
                return $this->getDollarEquivalence();
            case self::EURO:
                return $this->getEuroEquivalence();
            case self::UF :
                return $this->getUFEquivalence();
        }
        return 0;
    }
    
    private function getResource($service_name){
        return $this->http_client->request('GET', $service_name, ["query" =>$this->request_params] );
    }
    
    private function getDollarEquivalence(){
        try {
            $res = $this->getResource(self::SBIF_DOLAR_SERVICE);
            $jsonResponse = json_decode($res->getBody()->getContents());
            return $this->internationalizeNumber($jsonResponse->Dolares[0]->Valor);
        } catch (\Exception $ex) {
            return 0;
        }
    }
    private function getEuroEquivalence(){
        try {
            $res = $this->getResource(self::SBIF_EURO_SERVICE);
            $jsonResponse = json_decode($res->getBody()->getContents());
            return $this->internationalizeNumber($jsonResponse->Euros[0]->Valor);
        } catch (\Exception $ex) {
            return 0;

        }
    }
    private function getUFEquivalence(){
        try { 
            $res = $this->getResource(self::SBIF_UF_SERVICE);
            $jsonResponse = json_decode($res->getBody()->getContents());
            return $this->internationalizeNumber($jsonResponse->UFs[0]->Valor);
        } catch (\Exception $ex) {
            return 0;
        }
    }
    
    private function internationalizeNumber($number){
        return str_replace(",", ".", str_replace(".", "", $number));
    }
}
